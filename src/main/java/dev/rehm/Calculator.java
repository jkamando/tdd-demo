package dev.rehm;

public class Calculator {
    /*
    public double findAverage(double num1, double num2){
        return (num1+num2)/2;
    }

    public double findAverage(double num1, double num2, double num3){
        return (num1+num2+num3)/3;
    }
     */

    public double findAverage(double... nums){ // [] -> length = 0
        if(nums.length==0){
            throw new IllegalArgumentException("cannot invoke method with no numbers");
        }
        double sum = 0;
        for(double num: nums){
            sum += num;
        }
        return sum/nums.length;
    }
}
