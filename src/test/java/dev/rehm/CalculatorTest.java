package dev.rehm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {

    // input: 4, 6
    // output: 5
    @Test
    public void testAvgOfTwoNumbers(){
        Calculator calculator = new Calculator();
        double actual = calculator.findAverage(4,6);
        double expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void testAvgOfAPositiveAndNegativeValue(){
        Calculator calculator = new Calculator();
        double actual = calculator.findAverage(-4,4);
        double expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void testAvgOfThreeNumbers(){
        Calculator calculator = new Calculator();
        double actual = calculator.findAverage(5,10,15);
        double expected = 10;
        assertEquals(expected,actual);
    }

    @Test
    public void testAvgOfFourNumbers(){
        Calculator calculator = new Calculator();
        double actual = calculator.findAverage(5,10,15,14);
        double expected = 11;
        assertEquals(expected,actual);
    }

    @Test
    public void testEmptyArray(){
        Calculator calculator = new Calculator();
        // executing the findAverage method with no parameters throws an IllegalArgumentException
        assertThrows(IllegalArgumentException.class, ()->{
            calculator.findAverage();
        });
    }
}
